# Projet libre / open source Perlromaths

Attention, ce fichier README.md est un fichier README.md temporaire, le temps pour moi d'avoir une maîtrise suffisante et satisfaisante des différentes commandes Git depuis mon système d'exploitation OpenBSD (notamment concernant les branches, les tags,...).

Sachez que j'y travaille activement ! ;-)

## En attendant, voici quelques captures tronquées de ce que permet de produire le programme perlromaths :

![perlromaths01](./img/perlromaths01.png)

![perlromaths01c](./img/perlromaths01c.png)

![perlromaths02](./img/perlromaths02.png)

![perlromaths02c](./img/perlromaths02c.png)

![perlromaths03](./img/perlromaths03.png)

![perlromaths03c](./img/perlromaths03c.png)

![perlromaths04](./img/perlromaths04.png)

![perlromaths04c](./img/perlromaths04c.png)

![perlromaths05](./img/perlromaths05.png)

![perlromaths05c](./img/perlromaths05c.png)

## Crédits

Ce projet, créé de zéro, from scratch donc, dépend essentiellement du langage Perl (Practical Extraction Report Language) et de sa bibliothèque standard ainsi que du langage LaTeX (distribution TeX Live). 

Le Langage Perl a été choisi pour au moins les raisons suivantes : 

- Le langage Perl possède une syntaxe très permissive (contrairement à la syntaxe rigide du langage Python).

- Le langage (interprété) Perl possède une syntaxe proche du langage (compilé) C.

- Le langage Perl permet le développement assez rapide d'un projet de l'ampleur du projet Perlromaths, bien plus rapidement que si le langage C avait été utilisé.

- Le langage Perl permet nativement l'utilisation de REGEX (Regular Expressions), dans le langage même donc, de manière bien plus puissante et complète que les outils Unix sed ou awk. 

- Le langage Perl permet d'utiliser une forme simplifiée pour le programmeur de la notion de pointeurs du langage C, via des passages par référence, et ce, de manière transparente pour lui :  en effet, le langage Perl gère de lui même les allocations et les libérations des espaces mémoire.

- Le langage Perl est un vieux langage... Et bien, pourquoi pas, après tout ?!

Le projet Perlromaths n'utilise aucune bibliothèque (on parle de **module** dans le monde du projet Perl) du projet CPAN (Comprehensive Perl Archive Network). Les modules Perl intervenant dans le logiciel perlromaths ont été créés de zéro.

## Quels sont les langages informatiques utilisés dans le développement du programme perlromaths du projet Perlromaths ?

- Perl
- GNU make
- Shell
- LaTeX (distribution TeX Live) en s'appuyant sur les packages 'ProfCollege', 'ProfMaquette' et 'ProfLycee'.
- Python (indirectement par le biais du logiciel libre pyromaths et uniquement sur un système Debian GNU/Linux 12.7)

## Quelles sont les fonctionnalités du programme perlromaths ?

Le logiciel libre / open source perlromaths suit globalement la même philosophie que celle du projet Pyromaths, à savoir créer des exercices du cycle 3 et du cycle 4 du collège de manière pseudo-aléatoire suivis de leurs corrections.

En revanche, contrairement aux dernières versions du projet Pyromaths qui se sont tournées vers des technologies du Web (en lien avec Python de mémoire), le logiciel libre / open source perlromaths est un programme totalement CLI (Command Line Interface) développé avec les langages Perl, GNU make, LaTeX et Shell. 

Voici quelques raisons d'un tel choix qui peut paraître surprenant à l'ère du réseau des réseaux Internet, réseau souvent confondu avec ses applications Web :

- Je souhaitais un programme simple et efficace (philosophie KISS), pouvant être intégré facilement dans d'autres programmes tels que des scripts Shell par exemple (que ce soit le Korn shell pour les systèmes {Open|Free|Net}BSD ou que ce soit le shell Bash pour les systèmes GNU/Linux). Exemple très concret : Création aisée, de manière automatisée, sans intervention humaine, d'une trentaine de feuilles distinctes (a priori) d'exercices au format pdf avec leurs corrections.

- Je souhaitais un programme utilisable hors ligne (sans accès au réseau des réseaux Internet et donc sans contrainte pour l'utilisatrice de devoir lancer son navigateur Web Mozilla Firefox). Simple et efficace...

- Je souhaitais un programme dont je puisse facilement auditer le code source, un code que je connaisse assez bien pour en débusquer les bogues et ainsi les corriger mais aussi pour éviter toutes failles éventuelles de sécurité, ce qui exclut d'emblée l'utilisation de codes source tiers, notamment ceux provenant de technologies liées au Web.   

Le logiciel libre / open source perlromaths a aussi été conçu pour être portable (le langage Perl se trouve, comme le langage Python, dans la base system des systèmes de type Unix libre / open source que j'utilise au quotidien). Via une distribution Tex Live (à installer le cas échéant par le biais d'un gestionnaire de packages qui varie selon l'OS utilisé), le langage LaTeX est un autre élément essentiel concernant la portabilité du code source du projet Perlromaths. 

Aussi, actuellement, le programme perlromaths fonctionne uniquement sur les systèmes d'exploitation suivants (ceux que je peux utiliser au quotidien) :

- OpenBSD 7.5-stable
- FreeBSD 14.1-RELEASE-p5
- NetBSD 10.0-STABLE
- Debian GNU/Linux 12.7
- Arch Linux
- Slackware Linux 15.0

**Remarques :** 
- Attention, ne pas confondre la notion de logiciel **libre** et celle de logiciel **gratuit** .

- Le module Perl du logiciel perlromaths lié explicitement / spécifiquement au logiciel libre pyromaths ne fonctionne que sur un système Debian GNU/Linux 12.7, système d'exploitation sur lequel a été installé au préalable la dernière version en date du logiciel pyromaths. Aussi, ce module Perl ne fonctionne actuellement pas sur les cinq autres systèmes mentionnés ci-dessus. Pour surmonter ce manque, j'ai commencé un portage du logiciel pyromaths sur le système NetBSD, système de type BSD par principe le plus portable parmi les trois systèmes de type BSD cités ci-dessus, mais, ceci est une autre histoire, un autre projet libre / open source... ;-)

## Où est le code source du programme perlromaths ?

Lorsque cela sera prêt, il sera mis à disposition ici-même, sur ce dépôt Git hébergé sur cette instance GitLab du MENJ.

## Et quand cela sera prêt ?

**Réponse :** À l'instar du projet Debian : Quand ce sera prêt !

## Pourquoi perlromaths comme nom de programme ?

Il s'agit d'un clin d'oeil effectué à son digne ancêtre, le logiciel pyromaths, développé globalement en langage Python. Comme perlromaths est globalement développé en langage Perl, son nom se trouve être une **fusion** entre Perl et pyromaths.

## Licence : GNU GPLv3+

Comme le logiciel libre / open source perlromaths peut utiliser, si un utilisateur du système Debian GNU/Linux 12.7 le lui demande, du code source LaTeX généré par le langage Python du logiciel libre pyromaths, comme la licence du projet Pyromaths est la licence GNU GPLv3+, alors la licence du Projet Perlromaths s'est aussi portée sur la licence GNU GPLv3+. 
